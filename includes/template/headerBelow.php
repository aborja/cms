<?php

$belowLang=$trans->get('header_below');

?>
<div id="belowHeader" class="globalWidth ha-center l-mt-9">
  <div class="wrapper">
    <ul class="floatBox l-innerW-50">
      <li class="l-h-480">
        <figure style="background-image: url('images/belowHeaderBlock1Img.jpg')"></figure>
        <h1><?php echo $belowLang['title']; ?></h1>
        <a></a>
      </li>
      <li class="l-h-240 floatBox">
        <figure style="background-image: url('images/belowHeaderImg.jpg')"></figure>
        <span>
          <h1><?php echo $belowLang['subtitle_1']; ?></h1>
          <p><?php echo $belowLang['text_1']; ?></p>
          <a  href="https://www.youtube.com/watch?v=-zYaEAUOqPA&feature=youtu.be" class="lightbox"><?php echo $belowLang['watch']; ?></a>
        </span>
      </li>
      <li class="l-h-240 floatBox">
        <figure style="background-image: url('images/belowHeaderImg2.jpg')"></figure>
        <span>
          <h1><?php echo $belowLang['subtitle_2']; ?></h1>
          <p><?php echo $belowLang['text_2']; ?></p>
          <a href="?a=whyImport"><?php echo $belowLang['read_more']; ?></a>
        </span>
      </li>
    </ul>
  </div>
</div>