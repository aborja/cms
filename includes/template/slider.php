<?php 
 //lang key slider
 $sliderLang=$trans->get('slider');
?>
<header class="globalWidth ha-center l-h-600 l-mt-7">
  <div id="sliderBox">
    <div id="sliderBorders">
      <div></div>
      <div></div>
    </div>
    <div id="Arrows">
      <div class="Arrow1">
        <i class="fa fa-caret-left" aria-hidden="true"></i>
      </div>
      <div class="Arrow2">
        <i class="fa fa-caret-right" aria-hidden="true"></i>
      </div>
    </div>
    <ul>
      <li> 
        <div class="imgContainer">
          <figure style="background-image: url('images/slider/01.jpg')"></figure> 
        </div>   
        <div class="txtContainer">
          <span>
            <h1><?php echo $sliderLang['slide_title_1']; ?></h1>
            <p><?php echo $sliderLang['slide_text_1']; ?></p>
          </span>
        </div>
      </li>
      <li> 
        <div class="imgContainer">
          <figure style="background-image: url('images/slider/02.jpg')"></figure> 
        </div>   
        <div class="txtContainer">
          <span>
            <h1><?php echo $sliderLang['slide_title_2']; ?></h1>
            <p><?php echo $sliderLang['slide_text_2']; ?></p>
          </span>
        </div>
      </li>
      <li> 
        <div class="imgContainer">
          <figure style="background-image: url('images/slider/03.jpg')"></figure> 
        </div>   
        <div class="txtContainer">
          <span>
            <h1><?php echo $sliderLang['slide_title_3']; ?></h1>
            <p><?php echo $sliderLang['slide_text_3']; ?></p>
          </span>
        </div>
      </li>
    </ul>
  </div>
</header>