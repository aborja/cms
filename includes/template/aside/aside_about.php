<?php 
 $slideAboLang=$trans->get('aside_about');
?>
<aside class="l-w-25 m-w-100 m-mt-10">
  <div class="wrapper l-mb-15">
    <h1><?php echo $slideAboLang['title_1']; ?></h1>
    <figure style="background-image: url('images/asideMisionImg.jpg')"></figure>
    <p><?php echo $slideAboLang['text_1']; ?></p>
  </div>
  <div class="wrapper">
    <h1><?php echo $slideAboLang['title_2']; ?></h1>
    <figure style="background-image: url('images/asideVisionImg.jpg')"></figure>
    <p><?php echo $slideAboLang['text_2']; ?></p>
  </div>        
</aside>