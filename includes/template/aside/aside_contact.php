<?php 
$contactLang=$trans->get('contact');
?>
<aside id="asideContact" class="l-w-25 m-w-100 m-mt-10">
  <h1 class="title-1 l-fntS-20 l-mb-10"><?php echo $contactLang['title']; ?></h1>
  
  <div class="contactBox">
        <hgroup>
          <h1><?php echo $contactLang['smart_group_asia']; ?></h1>
          <h2><?php echo $contactLang['colombia']; ?></h2>
        </hgroup>
        <div class="infoBlock">
          <strong><?php echo $contactLang['address'] ?></strong>
          <p><?php echo $contactLang['colombia_dir'] ?></p>
        </div>
        <div class="infoBlock">
          <strong><?php echo $contactLang['phone']; ?></strong>
          <p><?php echo $contactLang['colombia_tel']; ?></p>
        </div>
        <div class="infoBlock">
          <strong><?php echo $contactLang['mail']; ?></strong>
          <p><?php echo $contactLang['colombia_mail']; ?></p>
        </div>
      </div>
      
      <div class="contactBox">
        <hgroup>
          <h1><?php echo $contactLang['smart_group_asia']; ?></h1>
          <h2><?php echo $contactLang['miami']; ?></h2>
        </hgroup>
        <div class="infoBlock">
          <strong><?php echo $contactLang['address'] ?></strong>
          <p><?php echo $contactLang['miami_dir'] ?></p>
        </div>
        <div class="infoBlock">
          <strong><?php echo $contactLang['mail']; ?></strong>
          <p><?php echo $contactLang['miami_mail']; ?></p>
        </div>
      </div>
      
      <div class="contactBox">
        <hgroup>
          <h1><?php echo $contactLang['smart_group_asia']; ?></h1>
          <h2><?php echo $contactLang['china1']; ?></h2>
        </hgroup>
        <div class="infoBlock">
          <strong><?php echo $contactLang['address'] ?></strong>
          <p><?php echo $contactLang['china1_dir'] ?></p>
        </div>
        <div class="infoBlock">
          <strong><?php echo $contactLang['mail']; ?></strong>
          <p><?php echo $contactLang['china1_mail']; ?></p>
        </div>
      </div>
      
      <div class="contactBox">
        <hgroup>
          <h1><?php echo $contactLang['smart_group_asia']; ?></h1>
          <h2><?php echo $contactLang['china2']; ?></h2>
        </hgroup>
        <div class="infoBlock">
          <strong><?php echo $contactLang['address'] ?></strong>
          <p><?php echo $contactLang['china2_dir'] ?></p>
        </div>
        <div class="infoBlock">
          <strong><?php echo $contactLang['mail']; ?></strong>
          <p><?php echo $contactLang['china2_mail']; ?></p>
        </div>
      </div> 
  
</aside>