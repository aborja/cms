<?php
//lang key menu
$topMenuLang = $trans->get('top_menu');
$menu = $trans->get('menu');
$footerLang = $trans->get('footer');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $configO->setTitle ?></title>

    <title>Compras y fabricantes en china, aseguramiento de calidad, logística y transporte</title>
    <meta name="description" content="Profesionales en compras en el mercado chino y países asiáticos. Especialistas en aseguramiento de calidad minimizando riesgos en la compra. Controlamos la fabricación de su proyecto en china desde la búsqueda de proveedores confiables hasta la entrega segura de su mercancía.">
    <meta name="keywords" content="negocios con Asia, negocios con china, controles de calidad, agendas de negocios, agenciamiento de compra, búsqueda de proveedores, qc, asesoría legal, controles de producción, envió de mercancía, manejo de inventarios, bodegaje en china, traducciones, facilitadores de negocios, agente comercial, documentos de exportación, apostillado, consularizacion de documentos, contratos, registro de marcas, desarrollo de marcas, desarrollo de productos, creacion de marcas propias, logística, nacionalización, venta, compra, importación, exportación, china, fabricantes chinos, productos chinos, feria de guangzhou, yiwu, calidad, mercancía, ferias, convenciones, shows, visa, electrodomésticos, escolar, automóviles, materias primas, insumos, juguetes, regalos, publicitarios. business with asia, businees with china, quality control, business agenda, business plan, purchase agents, sales agents, sourcing in china, procurement, legal assistance, quality control, product freight, insurance, inventory management, supply chain, translations to mandarin, comercial agents, expo, impo, contracts in asia, contracts in china, fair, exhibitions in asia, odm, lcl , labeling, hong kong, canton, shanghai.">

    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,600,600i,700,700i|Varela+Round" rel="stylesheet">    
    
  </head>
  <body>

    <div id="headerBg" class="l-w-100 
    <?php if ($a != home) { ?>
           l-h-175
         <?php } else { ?>
           l-h-660
         <?php } ?>
         ">
    </div>
    <nav id="menuP" class="l-h-200">
      <div class="wrapper globalWidth ha-center floatBox">
        <div class="borderR-rightTop"><div></div></div>
        <figure id="logoP" class="liquimg l-w-25 l-p-2">
          <img src="images/logoP.png" alt="Smart Group Asia">
        </figure>
        <div id="topOptions" class="floatBox">
          <div id="sessionBlock" class="l-mr-10">
            <a href="http://sys.sga.co" target="_blank"><i class="fa fa-user-plus"></i> <?php echo $topMenuLang['SGA_system']; ?></a>
          </div>
          <div id="langBlock" class="l-mr-8">
            <?php $lang = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'es'; ?>
            <?php if ($lang == "en"): ?>
              <a href="#" id="changeLang" data-lang="es"><i class="fa fa-language"></i> <?php echo $topMenuLang['spanish_Version']; ?></a>
            <?php else: ?>
              <a href="#" id="changeLang" data-lang="en"><i class="fa fa-language"></i> <?php echo $topMenuLang['english_version']; ?></a>
            <?php endif; ?>
          </div>
          <ul id="socialBlock" class="floatBox">      
            <li><a href="https://www.facebook.com/SMARTGROUPASIA" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/sga_smartgroup" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.instagram.com/smart_group_asia/" target="_blank"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
        <a id="nvBrs" href="#navContainer"></a>
        <div id="navContainer" class="l-w-72 l-ml-3 l-mt-9">
          <div class="darkBgZone"></div>
          <ul class="floatBox">
            <li><a href="?a=home"><?php echo $menu['home']; ?></a></li>
            <li><a href="?a=home#content"><?php echo $menu['about']; ?></a></li>
            <li><a href="?a=services"><?php echo $menu['services']; ?></a></li>
            <li><a href="?a=phases"><?php echo $menu['stages']; ?></a></li>
            <li><a href="?a=whyImport"><?php echo $menu['why_import_from_china']; ?></a></li>
            <li><a href="?a=successCase"><?php echo $menu['success_stories']; ?></a></li>
            <li><a href="?a=faq"><?php echo $menu['faqs']; ?></a></li>
            <li><a href="?a=ref"><?php echo $footerLang['commercial_references']; ?></a></li>
            <li><a href="?a=contact"><?php echo $menu['contact']; ?></a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div id="searchBlock" class="globalWidth ha-center l-h-0">
      <div class="inputbox" <?php if ($a != "home") {
              echo 'style="position: relative;bottom: -20px;"';
            } ?>>
        <i class="fa fa-search" aria-hidden="true"></i>
        <input type="search" <?php if ($a != "home") {
              echo 'style="width: 100%"';
            } ?> placeholder="¿Qué estás buscando?">
      </div>
    </div>

