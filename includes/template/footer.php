<?php
$footerLang = $trans->get('footer');
?>
</div>
<div id="aboveFooter" class="l-mb-8">
  <div class="wrapper globalWidth ha-center floatBox l-innerW-50 s-innerW-100">
    <div id="aFooterTxt">
      <h1><?php echo $footerLang['title']; ?></h1>
      <p><?php echo $footerLang['text']; ?></p>
    </div>
    <div id="aFooterInfo">
      <h1><?php echo $footerLang['phone']; ?></h1>
      <strong>(57 1) 551 31 31</strong>
    </div>
  </div>
</div>
<footer class="globalWidth ha-center floatBox">
  <div id="contact" class="l-w-40 s-w-100 l-mr-10">
    <figure class="liquimg l-mb-10">      
      <img src="images/logoP.png">
      <a href="https://www.camaracolombochina.com/" target="_blank" class="logoColombo" >
        <p><?php echo $footerLang['allies']; ?></p>
        <img src="images/logoColomboChina.jpg" alt=""/>
      </a>
    </figure>
    <div id="contactInfo">
      <h1><?php echo $footerLang['contact_information']; ?></h1>
      <div class="infoBlock">
        <h2><?php echo $footerLang['phone']; ?></h2>
        <p>(57 1) 551 31 31</p>
      </div>
      <div class="infoBlock">
        <h2><?php echo $footerLang['cell_phone']; ?></h2>
        <p>310 205 3810</p>
      </div>      
      <div class="infoBlock mail">
        <h2><?php echo $footerLang['email'] ?></h2>
        <p>servicioalcliente@sga.co</p>
      </div>
      <a href="?a=contact"><?php echo $footerLang['view_complete_information']; ?></a>
    </div>

  </div>
  <div id="navOptions" class="l-w-50 s-displayN floatBox">
    <nav class="l-w-45 l-mr-10">
      <h1><?php echo $footerLang['main_menu']; ?></h1>
      <ul>
        <li><a href="?a=home"><?php echo $footerLang['home']; ?></a></li>
        <li><a href="?a=phases"><?php echo $footerLang['stages']; ?></a></li>
        <li><a href="?a=successCase"><?php echo $footerLang['success_stories']; ?></a></li>
        <li><a href="?a=whyImport"><?php echo $footerLang['why_import_from_china']; ?></a></li>
        <li><a href="?a=faq"><?php echo $footerLang['faqs']; ?></a></li>
        <li><a href="?a=contact"><?php echo $footerLang['contact']; ?></a></li>
        <hr>        
        <li><a href="?a=ref"><?php echo $footerLang['commercial_references']; ?></a></li>
        <li><a href="?a=services"><?php echo $footerLang['services']; ?></a></li>
        <li><a href="http://sga.co/systems/Support6/webfactu/users/customer_login.php" target="_blank"><?php echo $footerLang['order_tracking'] ?></a></li>        
      </ul>
    </nav>
    <div id="novelty" class="l-w-40">
      <h1 class="l-fntS-21 l-ta-center"><?php echo $footerLang['negotiation_with_china']; ?></h1>
      <figure style="background-image: url('images/contentImg.jpg');">
      </figure>
      <p><?php echo $footerLang['negotiation_text_1']; ?></p>
      <a href="?a=phases"><?php echo $footerLang['read_more']; ?></a>
    </div>
    <div id="footerTags" class="l-w-100">
      <h1><?php echo $footerLang['negotiation_text_2']; ?></h1>
    </div>
  </div>
</footer>

<script language="javascript" src="js/JQuery.js"></script>
<script language="javascript" src="js/JQuery-UI.js"></script>
<script language="javascript" src="js/JQueryTransit.min.js"></script>
<script language="javascript" src="js/Slider.js"></script>
<script language="javascript" src="js/language.js"></script>

<link rel="stylesheet" type="text/css" href="Lightbox Evolution/js/lightbox/themes/default/jquery.lightbox.css" />
<!--[if IE 6]>
<link rel="stylesheet" type="text/css" href="../Lightbox Evolution/js/lightbox/themes/default/jquery.lightbox.ie6.css" />
<![endif]-->
<script type="text/javascript" src="Lightbox Evolution/js/lightbox/jquery.lightbox.min.js"></script>

<script type="text/javascript">
  jQuery(document).ready(function ($) {
    $('.lightbox').lightbox();
  });
</script>

<script>
  $(document).on("click", "#nvBrs", function () {
    $("#navContainer").css({"visibility": "visible"});
    $("#navContainer .darkBgZone ").animate({"opacity": "1"});
    $("#navContainer ul").animate({"left": "0"});

  });
  $(document).on("click", "#navContainer .darkBgZone", function () {
    $("#navContainer .darkBgZone ").animate({"opacity": "0"});
    $("#navContainer ul").animate({"left": "-290px"}, function () {
      $("#navContainer").css({"visibility": "hidden"});
    });
  });

</script>

</body>
</html>