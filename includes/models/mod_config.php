<?php

class Config{
  
  var $setPath;
  var $setView;
  var $setAside = "contact";
  var $setTitle;
  
  function setPath($p){
    $this->setPath = ($p);
    return $this -> setPath;
  }
  
  function setView($v){
    $this->setView = ($v);
    return $this -> setView;
  }
  
  function setAside($s){
    $this->setAside = ($s);
    return $this -> setAside;
  }
  
  function setTitle($t){
    $this->setTitle = ($t);
    return $this -> setTitle;
  }
  
}