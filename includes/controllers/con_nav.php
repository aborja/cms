<?php

switch ($a) {
  case "home":
   
    $viewLang=$trans->get($_REQUEST['a']);
      
    $configO->setAside("about");
    $configO->setTitle("Compras y fabricantes en china, aseguramiento de calidad, logística y transporte");
    break;
  
  case "phases":
    $viewLang=$trans->get($_REQUEST['a']);  
    $configO->setAside(false);
    $configO->setTitle("Proceso de Negociación con China");
    break;
  
  case "successCase":
    $viewLang=$trans->get($_REQUEST['a']);    
    $configO->setTitle("Casos de Éxito");
    break;
  
  case "faq":
     $faqLang=$trans->get($_REQUEST['a']);
    $configO->setTitle("Preguntas frecuentes");
    break;
  
  case "whyImport":
    $configO->setTitle("Porqué importar de China");
    break;
  
  case "contact":
    $configO->setAside(false);
    $configO->setTitle("Contáctenos - SGA");
    break;
  
  case "services":
    $viewLang=$trans->get($_REQUEST['a']);    
    $configO->setTitle("Servicios - SGA");
    break;
  
  case "ref":
    $configO->setAside(false);    
    $configO->setTitle("Referencias Comerciales");
    break;
    
  case "setLanguage":
      $_SESSION["lang"]=$_REQUEST['lang'];
      $message=['success'=>'true'];
      echo json_encode($message);
      exit();
  break;    
}