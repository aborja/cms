<?php

include 'includes/models/mod_config.php';

$a = $_GET['a'];

if (empty($a)) {
  $a = 'home';
  header('Location:'."?a=home");
}

$configO = new Config();

$configO->setPath($a);
$configO->setView($a);

if (isset($a)) {
  include 'includes/controllers/con_nav.php';
}

if (isset($a)) {
  include 'includes/template/header.php';
  
  if ($a == "home") {
    include 'includes/template/slider.php';
    include 'includes/template/headerBelow.php';
  }
  
  echo '<div id="content" class="globalWidth floatBox ha-center l-mt-4 s-mt-10 l-mb-6"><main ';
  if ($configO->setAside) {
    echo 'style="border-right: 1px solid rgba(0,0,0,0.1);" class="l-w-68 m-w-100 l-pr-7 l-mr-7 m-pr-0 m-mr-0">';
  } else {
    echo 'class="l-w-100">';
  }  
  include 'includes/views/' . $configO->setPath . '/view_' . $configO->setView . '.php';
  echo '</main>';

  if ($configO->setAside) {
    include 'includes/template/aside/aside_'. $configO->setAside .'.php';
  }

  include 'includes/template/footer.php';
}