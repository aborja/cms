<section id="faq">
  <article>
    <h1 class="title-1 l-fntS-40 l-mb-6"><?php echo $faqLang['title']; ?></h1>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_1']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_1']; ?> </p>

    <h1 class="title-1 l-fntS-30 l-mb-6"><?php echo $faqLang['title_2']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_2']; ?></p>

    <h1 class="title-1 l-fntS-30 l-mb-6"><?php echo $faqLang['title_3']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_3']; ?></p>
    
     <h1 class="title-1 l-fntS-25 l-mb-1"><?php echo $faqLang['title_4']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_4']; ?></p>

    <h1 class="title-1 l-fntS-25 l-mb-1"><?php echo $faqLang['title_6']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_6']; ?></p>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_7']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_7']; ?></p>
    
    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_8']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_8']; ?></p>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_9']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_9']; ?></p>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_10']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_10']; ?></p>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_11']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_11']; ?></p>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_12']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_12']; ?></p>
    
    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_13']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_13']; ?></p>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_14']; ?></h1>
     <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_14']; ?></p>
    
    <h1 class="title-1 l-fntS-25 l-mb-6"> <?php echo $faqLang['title_15']; ?></h1>
    <ul class="list-1 l-mb-6">
      <?php echo $faqLang['list']; ?>
    </ul>

    <h1 class="title-1 l-fntS-25 l-mb-6"><?php echo $faqLang['title_16']; ?></h1>
    <p class="txt-1 l-fntS-18 l-mb-3"><?php echo $faqLang['text_16']; ?></p>
    
    <ul class="list-1 l-mb-6">
      <li><?php echo $faqLang['list_text2_1']; ?></li>
      <li><?php echo $faqLang['list_text2_2']; ?></li>
      <li><?php echo $faqLang['list_text2_3']; ?></li>
    </ul> 

    <p class="txt-1 l-fntS-18 l-mb-6"><?php echo $faqLang['text_17']; ?></p>

  </article>
</section> 