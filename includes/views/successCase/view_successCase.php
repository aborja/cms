<?php 
  $successCaseLang=$trans->get('successCase');
?>
<section id="successCase">
  <article>
    <h1 class="title-1 l-fntS-40"><?php echo $successCaseLang['title']; ?></h1>
    <h2 class="subtitle-1 l-fntS-24 l-mb-5"><?php echo $successCaseLang['subtitle']; ?></h2>
    <div class="wrapper">
      <div class="successBox">
        <figure><img src="images/successCase/rti.jpg"></figure>
        <div>
          <?php echo $successCaseLang['case1']; ?>
        </div>
      </div>
      <div class="successBox">
        <figure><img src="images/successCase/pettacci.jpg"></figure>
        <div>
          <?php echo $successCaseLang['case2']; ?>
        </div>
      </div>
      <div class="successBox">
        <figure><img src="images/successCase/cd_systems.jpg"></figure>
        <div>
          <?php echo $successCaseLang['case3']; ?>
        </div>
      </div>
      <div class="successBox">
        <figure><img src="images/successCase/lg.jpg"></figure>
        <div>
          <?php echo $successCaseLang['case4']; ?>
        </div>
      </div>
      <div class="successBox">
        <figure><img src="images/successCase/humcar.jpg"></figure>
        <div>
          <?php echo $successCaseLang['case5']; ?>
        </div>
      </div>
    </div>
  </article>
</section> 