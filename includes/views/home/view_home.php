<section>
  <a name="content"></a>
  <article>
    <h1 class="title-1 l-fntS-30 l-mb-5">Nuestra Historia</h1>
    <p class="txt-1 l-fntS-18">
      <span class='img-1 l-w-40 s-w-100 l-pt-50 l-mr-7 s-mr-0 l-mb-13' style="background-image: url('images/contentImg.jpg')"></span>
      <?php echo $viewLang['text_history'] ?>
    </p>
  </article>
</section> 