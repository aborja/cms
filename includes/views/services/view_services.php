<?php
//lang key menu
$serv= $trans->get('services');
?>

<section id="services">
  <article>
    <h1 class="title-1 l-fntS-40 l-mb-6"><?php echo $viewLang['title'] ?></h1>

    <ul>
      <li>
        <a>
          <figure><img src="images/services/certificate.png"></figure>
          <h3><?php echo $serv['serv1']['title'] ?></h3>
<!--          <p><?php echo $serv['serv1']['text'] ?></p>-->
        </a>
      </li>

      <li>
        <a>
          <figure>
            <img src="images/services/plan.png">
          </figure>
          <h3><?php echo $serv['serv2']['title'] ?></h3>
<!--          <p><?php echo $serv['serv2']['text'] ?></p>-->
        </a>
      </li>

      <li>
        <a>
          <figure><img src="images/services/translation.png"></figure>
          <h3><?php echo $serv['serv3']['title'] ?></h3>
          <p><?php echo $serv['serv3']['text'] ?></p>
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/passport-card.png"></figure>
          <h3><?php echo $serv['serv4']['title'] ?></h3>
          <p><?php echo $serv['serv4']['text'] ?></p>
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/eye-open.png"></figure>
          <h3><?php echo $serv['serv5']['title'] ?></h3>
          <p><?php echo $serv['serv5']['text'] ?></p>
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/businessman.png"></figure>
          <h3><?php echo $serv['serv6']['title'] ?></h3>
          <p><?php echo $serv['serv6']['text'] ?></p>
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/tick-inside-circle.png"></figure>
          <h3><?php echo $serv['serv7']['title'] ?></h3>
<!--          <p><?php echo $serv['serv7']['text'] ?></p>-->
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/agency-connection.png"></figure>
          <h3><?php echo $serv['serv8']['title'] ?></h3>
          <p><?php echo $serv['serv8']['text'] ?></p>
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/box.png"></figure>
         <h3><?php echo $serv['serv9']['title'] ?></h3>
          <p><?php echo $serv['serv9']['text'] ?></p>
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/magnifier-tool.png"></figure>
          <h3><?php echo $serv['serv10']['title'] ?></h3>
          <p><?php echo $serv['serv10']['text'] ?></p>
        </a>
      </li>
      <li>
        <a>
          <figure><img src="images/services/inspection.png"></figure>
          <h3><?php echo $serv['serv11']['title'] ?></h3>
          <p><?php echo $serv['serv11']['text'] ?></p>
        </a>
      </li>
        
    </ul>

  </article>
</section> 