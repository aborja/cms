<?php 
  $ref=$trans->get('contactt');
?>
<section id="references">
  <article>
    <h1 class="title-1 l-fntS-40">Referencias Comerciales</h1>
    <h2 class="subtitle-1 l-fntS-24 l-mb-5">SMART GROUP ASIA CORP.</h2>
    <div class="wrapper">

      <div class="refBox">
        <figure>
          <img src="images/references/agroCampo.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Agrocampo</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Luis Felipe Baron</li>
          <li><strong><?= $ref['position'] ?>: </strong>Director De Importaciones</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 73 No. 20-62</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 326 56 60 </li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/fedco.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Aci Fedco</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Lina Maya</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 101 N 70g -83</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 226 06 28</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/leBon.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Inscra S.A</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Eliana Palacio</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cra. 48 # 98 A S 500</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 4) 448 99 88</li>
          <li><strong><?= $ref['city'] ?>: </strong>Estrella</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/house.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Arias Y Cia Ltda.</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Mauricio Arias</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente General</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cll. 16 No. 14 -44</li>
          <li><strong><?= $ref['phone'] ?>: </strong>315 540 91 65</li>
          <li><strong><?= $ref['city'] ?>: </strong>Armenia</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/cdSystem.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>CD Systems De Colombia</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Victor Manuel Jaramillo</li>
          <li><strong><?= $ref['position'] ?>: </strong>Director Administrativo Y Financiero</li>
          <li><strong><?= $ref['address'] ?>: </strong>Carrera 12 # 23-56</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 821 50 00</li>
          <li><strong><?= $ref['city'] ?>: </strong>Funza (Cundinamarca)</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/presh.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>PRESH TECH SAS</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Juan Carlos Sanchez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente General</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 12 # 68 C - 30</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 447 95 95</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>       
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/pinturasEvery.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Pinturas Every</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Felipe Grisales</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente General</li>
          <li><strong><?= $ref['address'] ?>: </strong> Km 1.8 Via Madrid - Subachoque</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 546 05 05</li>
          <li><strong><?= $ref['city'] ?>: </strong>Madrid Cundinamarca</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/hardBody.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Hard Body</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Maria Del Rosario Gutierrez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente General</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 109 N° 14ª - 34</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 633 19 99 / (57 1) 861 55 77</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/intergraficas.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Intergraficas Sas</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Boris Visbal</li>
          <li><strong><?= $ref['position'] ?>: </strong>Director Financiero</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cra 12 # 23 56</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 821 50 00</li>
          <li><strong><?= $ref['city'] ?>: </strong>Funza, Colombia</li>
        </ul>
      </div>


      <div class="refBox">
        <figure>
          <img src="images/references/creArte.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Crearte Promocionales</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Victor Manuel Jaramillo</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente General</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cra 12 # 79-08 Of 503</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57) 317 427 84 82</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/disalud.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Ortopedicos Disalud</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Luis Gerardo Tovar</li>
          <li><strong><?= $ref['position'] ?>: </strong>Director General</li>
          <li><strong><?= $ref['address'] ?>: </strong>Transversal 74 F No. 40-23 Sur</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 470 24 71 / 310 860 03 29 </li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/ecoTrade.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Ecotrade Sas</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Wilson Javier Moreno</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente General</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 24 A # 57-69 </li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57) 322 213 61 28</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/asiaBConsulting.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Asia B Consulting</li>
          <li><strong><?= $ref['contact'] ?>: </strong>David Barriga</li>
          <li><strong><?= $ref['position'] ?>: </strong>Presidente Asia B Consulting</li>          
          <li><strong><?= $ref['address'] ?>: </strong>Carrera 13 N° 75-20 Ofc. 607</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 317 83 60 / 57 313 381 87 59</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/proyectosIndustriales.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Proyectos Industriales Ltda</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Antonio Bohorquez Fernandez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Arquitecto</li>
          <li><strong><?= $ref['address'] ?>: </strong>Carrera 8 N° 44b – 80 Barrio El Troncal</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 2) 445 71 72 / (57 2) 445 71 77</li>
          <li><strong><?= $ref['city'] ?>: </strong>Cali, Colombia</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/medighan.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Medighan</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Allison Tovar</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente General</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 50 N° 13 – 19 Ofc. 401</li>
          <li><strong><?= $ref['phone'] ?>: </strong>314 427 29 01</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá<li>
        </ul>
      </div>      
      
      <div class="refBox">
        <figure>
          <img src="images/references/farmaciaInstitucional.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Farmacia Institucional</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Katherine Ruiz</li>
          <li><strong><?= $ref['position'] ?>: </strong>Representante Gerencia</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cra 64 N°67d - 05</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 604 50 26</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/zar.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Ingenieria Zar</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Jaime Cortazar</li>
          <li><strong><?= $ref['position'] ?>: </strong>Representante</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cra 19 A N° 106ª - 42</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 619 71 20 / 310 233 16 85</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/constructoraCRD.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Constructora CRD S.A.</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Hector Maldonado Barrios </li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 44b N° 57ª - 76</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 316 01 64 / 57 316 330 11 84</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/amavia.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Constructora Amavia Sas</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Catalina Fuquen Guerrero</li>
          <li><strong><?= $ref['address'] ?>: </strong>Carrera 57b N° 127c - 49</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 479 57 67</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/abril.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Abril Constructora </li>
          <li><strong><?= $ref['contact'] ?>: </strong>Diego Abril</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Carrera 15 N° 52ª – 06 Pi 6</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 346 68 16 / (57 1) 595 89 80</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/cubidLed.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Cubid Led</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Victoria Avila Vasquez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente Comercial</li>
          <li><strong><?= $ref['address'] ?>: </strong>Av Calle 116 N° 22-45 Apto 102</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 215 82 16 / 310 551 33 66</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/IBH.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>IBH  Construcciones</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Iader Barrios Hernandez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Director</li>
          <li><strong><?= $ref['address'] ?>: </strong>Carrera 19 N°84  41 Ofc 202</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 634 86 08</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/pahi.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Pahi</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Juan Manuel Pava</li>
          <li><strong><?= $ref['position'] ?>: </strong>Director</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cra 121b No 5 101 Casa 5clle 42 14-53 Chapinero (Local)</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 441 10 56</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/pettacci.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Pettacci International</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Laureano Núñez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Zona Franca Cra. 106 No. 15-25</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 439 54 66 – 315 296 90 42</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/jgElectronic.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Jg Electronic Ltda</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Miguel Canal</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cr. 33 No. 98 - 44</li>
          <li><strong><?= $ref['phone'] ?>: </strong>311 480 78 70 - 571 622 26 11 – 571 691 34 75</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/montajesServicios.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Montajes Y Servicios </li>
          <li><strong><?= $ref['contact'] ?>: </strong>Francisco Collavini</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 64 Nº 50 – 22 Of. 101</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(5) 344 31 82</li>
          <li><strong><?= $ref['city'] ?>: </strong>Barranquilla</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/dcalidad.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>D’calidad</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Dario Cárdenas Torres</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 16 Nº 19 – 05 Sur Piso 2º</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 366 50 81</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/surtiAlarmas.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Surtialarmas</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Hugo Cruz</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cr. 10 No. 21 – 06 Ofc. 372</li>
          <li><strong><?= $ref['phone'] ?>: </strong>315 797 84 59 - (57 1) 347 45 55 – (57 1) 283 48 92</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/pareForros.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Pare Forros</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Edgar Rueda</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Av. Carrera 30 No. 45 A - 18</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 269 07 39</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/zimmer.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Zimmer S.A.</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Rosa Bustamante </li>
          <li><strong><?= $ref['position'] ?>: </strong>Directora Administrativa </li>
          <li><strong><?= $ref['address'] ?>: </strong>Cll. 10 No. 30-30</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 4) 268 39 00 /  312 288 80 28 </li>
          <li><strong><?= $ref['city'] ?>: </strong>Medellín</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/variadores.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Variadores S.A.</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Jorge Enrique Tobon</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Av. 37 (Ciudad De Quito)No. 82-05</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 635 72 88</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/nacionalDeSellos.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Nacional De Sellos</li>
          <li><strong><?= $ref['contact'] ?>: </strong>German Ruiz</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>          
          <li><strong><?= $ref['address'] ?>: </strong>Cra. 43ª No. 22b-80</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 269 88 00</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img src="images/references/aquiles.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Aquiles S.A.</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Alvaro Ruiz</li>
          <li><strong><?= $ref['position'] ?>: </strong>Asisitente De Gerencia</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 20 No 43 33</li>
          <li><strong><?= $ref['phone'] ?>: </strong>311 219 64 55</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/openHouse.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Open House</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Jose Luis  Perilla</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 70 A Nº  15 - 16</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 217 86 43</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>
      
      <div class="refBox">
        <figure>
          <img src="images/references/dentalesAmerica.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Dentales América Ltda</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Teddy López</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cra. 17 Nº 58 - 22</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 235 40 69</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotà</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/aquaServicios.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Aquaservicios</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Jhon Jairo Villa</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 48 C Nº 21 . 44</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(6) 885 14 27</li>
          <li><strong><?= $ref['city'] ?>: </strong>Manizales</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/carBus.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Car & Bus Ltda</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Carolina Galarza</li>
          <li><strong><?= $ref['position'] ?>: </strong>Coordinadora De Suministros</li>
          <li><strong><?= $ref['address'] ?>: </strong>Dg. 53 Nº 56 – 89</li>
          <li><strong><?= $ref['phone'] ?>: </strong> (57 1) 324 42 42</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>      

      <div class="refBox">
        <figure>
          <img>
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Villa Colombia E.U.</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Andrea Sanchez Y/O Oscar Sanchez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Cll.  44 D Nº 45– 45 </li>
          <li><strong><?= $ref['phone'] ?>: </strong> (57 1) 368 83 05 / (57 1) 315 48 71</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

      <div class="refBox">
        <figure>
          <img src="images/references/superDeko.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Superdeko</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Gabriel Gómez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['address'] ?>: </strong>Calle 33 Sur Nº 46 – 19 Piso 3º</li>
          <li><strong><?= $ref['phone'] ?>: </strong> (57 1) 724 17 05 / (57 1) 255 19 82</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div> 
      
      <div class="refBox">
        <figure>
          <img src="images/references/ingenieriaDeServicios.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>CJL Ingeniería De Servicios</li>
          <li><strong><?= $ref['contact'] ?>: </strong>Carlos Julio Lizarazo</li>
          <li><strong><?= $ref['position'] ?>: </strong>Gerente</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1) 713 76 52</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>     
      
      <div class="refBox">
        <figure>
          <img src="images/references/texin.jpg">
        </figure>
        <ul>
          <li><strong><?= $ref['company'] ?>: </strong>Texin Ltda</li>
          <li><strong><?= $ref['contact'] ?>: </strong>William Hernandez</li>
          <li><strong><?= $ref['position'] ?>: </strong>Director Administrativo Y Financiero</li>
          <li><strong><?= $ref['address'] ?>: </strong>Call 74 A No 53 54</li>
          <li><strong><?= $ref['phone'] ?>: </strong>(57 1)  660 64 30 / (57 1)  237 73 91</li>
          <li><strong><?= $ref['city'] ?>: </strong>Bogotá</li>
        </ul>
      </div>

    </div>
  </article>
</section> 