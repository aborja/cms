<?php 
 $whyimportLang=$trans->get('whyimport');
?>
<section>
  <article>
    <h1 class="title-1 l-fntS-40"><?php echo $whyimportLang['title']; ?></h1>
    <h2 class="subtitle-1 l-fntS-24 l-mb-5"></h2>
    <p class="txt-1 l-fntS-18 l-mb-5">
      <span class='img-1 l-w-40 l-pt-35 l-mr-7 l-mb-5' style="background-image: url('images/contentImg.jpg')"></span><?php $whyimportLang['text_1'] ?>
    </p>

    <p class="txt-1 l-fntS-18 l-mb-5"><?php echo $whyimportLang['text_2']; ?></p>

    
  </article>
</section> 