<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Lang{
 
    
    public function get($key){
        return $this->text($key);
    }
    
    private function text($key){
        
       $text= [
           'contactt'=>[
               'company'=>'Empresa',
               'contact'=>'Contacto',
               'position'=>'Cargo',
               'address'=>'Dirección',
               'phone'=>'Teléfono',
               'city'=>'Ciudad'
           ],
           
           'general'=>[
               'allies'=>'Allies with'
           ],
           'top_menu'=>[
             'SGA_system'=>'Seguimiento de órdenes',
             'spanish_Version'=>'Spanish Version',  
             'english_version'=>'English Version'
           ],
           'menu'=>[
              'home'=>'Inicio',
               'services'=>'Servicios',
              'stages'=>'Etapas',
               'success_stories'=>'Casos de Éxito',
               'why_import_from_china'=>'Porqué importar de China?',
               'faqs'=>'FAQ´s',
               'contact'=>'Contacto',
               'about'=>'Nosotros'
           ],
            'slider'=>[
                'slide_title_1'=>'Qué ofrece SMART GROUP ASIA a los empresarios?',
                'slide_text_1'=>'Ofrecemos un acompañamiento total en procesos de compra dentro del continente Asiático para productos terminados, semi-terminados y de materias primas. Nuestra trayectoria de más de diez años en la República Popular China, nos ha posicionado como líderes del mercado, convirtiéndonos en parte integral del crecimiento de importantes empresas y marcas.',
                'slide_title_2'=>'Qué ofrece SMART GROUP ASIA a los empresarios?',
                'slide_text_2'=>'La asesoría que prestamos a nuestros aliados comerciales, tiene como finalidad reducir los riesgos que existen al momento de hacer compras en Asia. Velamos por los intereses de nuestros clientes representándolos ante sus proveedores y entendiendo las necesidades de sus órdenes, para luego asegurarnos que reciban el producto requerido, en el debido tiempo, con los estándares de calidad deseados y dentro de las expectativas de costo.',
                'slide_title_3'=>'Qué ofrece SMART GROUP ASIA a los empresarios?',
                'slide_text_3'=>'Contamos con un sistema de seguimiento de órdenes desarrollado a la medida, que nos permite tener el máximo control sobre órdenes y mantener un seguimiento de cada proceso desde la recepción de la orden hasta la entrega del producto en destino.'
            ],
           'header_below'=>[
               'title'=>'Grupo SGA',
               'subtitle_1'=>'Quienes confían en nosotros',
               'text_1'=>'',
               'watch'=>'Ver video',
               'read_more'=>'Leer más',
               'subtitle_2'=>'Porqué importar de China?',
               'text_2'=>'Teniendo en cuenta que las grandes empresas de todo el mundo han fijado la atención en la manufactura China desde hace tanto tiempo, podemos entender que las fábricas del gigante Asiático...'
               
           ],
           'aside_about'=>[
               'title_1'=>'Misión',
               'text_1'=>'Prestar el mejor servicio de consultoría especializada en compras en el mercados asiático, con el fin de afianzar y enriquecer las relaciones comerciales entre nuestros clientes y sus proveedores, minimizando riesgos en la compra y venta de bienes, brindando una asistencia responsable, ágil, eficiente y con los mayores estándares de calidad.',
               'title_2'=>'Visión',
               'text_2'=>'Ser la compañía líder y mas innovadora en consultoría en compras entre América Latina y el continente Asiático. Pretendemos convertirnos en una herramienta fundamental para el crecimiento y posicionamiento de nuestros clientes y sus marcas, comprometidos con el bienestar y desarrollo de empleados y accionistas.'
           ],
           'footer'=>[
               'allies'=>'Allies with',
               'title'=>'Somos su mejor opción',
               'text'=>'"Valores como la especialización, sistematización de procesos, excelencia, transparencia, y prestación de servicios de calidad con un alto valor agregado, nos han permitido convertirnos en una multinacional de servicios comerciales y garantizan los mejores resultados para su compañía."',
               'phone'=>'Teléfono',
               'contact_information'=>'Datos de contacto',
               'cell_phone'=>'Celular',
               'email'=>'Email',
               'view_complete_information'=>'Ver información completa',
               'main_menu'=>'Menú Principal',
               'home'=>'Home',
               'stages'=>'Etapas',
               'success_stories'=>'Casos de Éxito',
               'why_import_from_china'=>'Por que importar de China?',
               'faqs'=>'FAQ´s',
               'contact'=>'Contacto',
               'commercial_references'=>'Nuestros clientes',
               'services'=>'Servicios',
               'order_tracking'=>'Seguimiento de órdenes',
               'negotiation_with_china'=>'Negociación con China',
               'negotiation_text_1'=>'Conocer todas las etapas y el proceso de negociación con el gigante asiático es de vital importancia. Por eso, hemos desarrollado una infografía que le ayudará a visualizar en detalles todo el proceso desde la recepción de la orden hasta la entrega de su producto en destino.',
               'read_more'=>'Leer Más',
               'negotiation_text_2'=>'Profesionales en compras en el mercado Asiático con énfasis en China. Especialistas en aseguramiento de calidad, minimizando riesgos en los procesos de compra. Controlamos la fabricación de su proyecto en China desde la búsqueda de un proveedor o producto confiable, hasta la entrega segura de su mercancía.'
               
           ],
            'home'=>[
                'our_history'=>'Nuestra Historia',
                'text_history'=>'Smart Group Asia, es una compañía de consultaría internacional en compras, enfocada en los mercados Asiáticos, con sus oficinas principales en La Republica Popular China en las ciudades Yiwu y GuangZhou, oficinas en Bogotá y Medellín (Colombia) y oficina de representación en Miami (Estados unidos). <br><br>

Nos especializamos en el análisis, desarrollo e implementación de proyectos en el área de comercio internacional, enfocados en soluciones de búsqueda de proveedores y productos, outsourcing para producción, representación de compañías, creación de marcas propias, despacho y nacionalización de mercancías.<br><br>


Desde nuestros inicios en el año 2006 generamos una visión clara y enfocada hacia el crecimiento en el mercado, gracias a nuestra experiencia, hemos desarrollado numerosos casos de éxito en compras de productos terminados, semiterminados, materias primas, desarrollo de producto y creación de marca en los mercados Asiáticos.<br><br>

Contamos con la infraestructura y experiencia para configurarnos como una de las  empresas más solidas y profesionales que prestan servicios de  agenciamiento de compra en y hacia Asia.<br><br>

Hemos logrado desarrollar los objetivos de nuestros clientes prestando un servicio con altísima calidad, respaldo y profesionalismo. Para Smart Group Asia el activo más representativo para alcanzar las metas de sus clientes son sus empleados, quienes han sido capacitados en la búsqueda de proveedores, procesos de negociación, manejo de tiempos, control de calidad, administración de inventarios, procesos de nacionalización de mercancías, legislatura comercial, despachos y entrega de producto terminado, utilizando las más avanzadas técnicas, metodologías y buenas prácticas en importación y exportación de productos, con el fin de ofrecer servicios integrales y soluciones totales. Hemos seleccionado cuidadosamente un grupo de personas con habilidades de comunicación en Mandarín, Inglés y español, con el fin de tener un mejor entendimiento de nuestros clientes y sus necesidades, además de ser efectivos al momento de comunicarnos con proveedores. <br><br>

El éxito de nuestro equipo de consultores, es producto de la Innovación, Investigación y mejoramiento continuo de nuestros productos y servicios.
'
            ],
            'phases'=>[
                'img_pashes'=>'images/phases.jpg',
                'img_mpashes'=>'images/m.phases.jpg'
            ],
            'successCase'=>[
                'title'=>'Casos de éxito',
                'subtitle'=>'Lo que dicen nuestros clientes',
                'case1'=>'<p>Elegimos a  SGA Smart Group Asia ya que nos ha permitido contar con el respaldo y profesionalismo a la hora de hacer negocios con Asia , hemos realizado un sin numero de importaciones , contando con excelente calidad de nuestros productos y tiempos de transito perfectos.</p>
                            <span>
                              <strong>RTI Television S.A</strong>
                              <br>Luis Alberto Monroy.<br>
                              Gerente General.
                            </span>',
               'case2'=>'<p>Como Fabricantes y exportadores ser competitivos y promover la fabricacion y el empleo puntos en los cuales SGA Smart Group Asia nos ha apoyado desde el ano 2005, hemos realizado un sin numero de importaciones de materias primas para el desarrollo e innovacion de nuestros productos.</p>
                        <span>
                          <strong>Pettacci S.A</strong>  <br>          
                          Laureano Nunez Baron.<br>
                          Gerente General.
                        </span>',
               'case3'=>'<p>SGA, presta un servicio muy profesional , el cual nos ha permitido un crecimiento sostenido desde el ano 2007 , y un nivel alto de comunicacion y agilidad en las importaciones de nuestras materias primas y productos terminados. hoy hacer negocios con Asia es vital para nuestro negocio y con una herramienta tan eficiente y segura como SGA Smart Group Asia minizamos todos los riesgos a la hora de realizar cualquier tipo de negociacion.</p>
                        <span>
                          <strong>CD Systems S.A.</strong><br>
                          Gabriel Cortzar.<br>
                          Gerente General.
                        </span>',
               'case4'=>'<p>SGA Smart Group Asia es una herramienta Util para realizar efectivamente cualquier tipo de negociacion con Asia , cuentan con oficinas y personal capacitado para desarrollar cualquier tipo de negocio.  Una de las cosas que mas nos gusta de SGA es su capacidad de respuesta , inspecciones de calidad certificadas y  la seguridad que hemos enviado sumas considerables de dinero y durante estos 6 anos nunca hemos tenido ningun inconveniente.</p>
                        <span>
                          <strong>JG Electronics Ltda</strong><br>
                          Miguel Canal.<br>
                          Gerente General.
                        </span>',
               'case5'=>'<p>Estabamos buscando una empresa que nos permitiera Garantizar La calidad de nuestros productos importados desde Asia, de una manera profesional y gracias a ,  SGA Smart Group Asia,  optenemos un informe detallado previo al despacho de la mercancia y podemos corregir fallas de produccion para garantizar y minizar todo riesgo en la compra...</p>
                        <span>
                          <strong>Industrias Humcar Ltda </strong><br><br>
                          Martin Camacho<br>
                          Gerente General.
                        </span>'
               
            ],
           'whyimport'=>[
               'title'=>'¿Porqué importar de China?',
               'text_2'=>'Teniendo en cuenta que las grandes empresas de todo el mundo han fijado la atención en la manufactura China desde hace tanto tiempo, podemos entender que las fábricas del gigante Asiático están en capacidad de crear no solo una gran variedad de oferta de productos, sino que a través de la especialización, modernización de sus procesos productivos, sus ventajas comparativas de mano de obra y beneficios fiscales, han mejorado notablemente la calidad de los mismos.<br><br>

China se erige como una de las economías mas influyentes del planeta y presenta un mercado lleno de oportunidades que no solo cautiva sino que involucra a un sinnúmero de empresas alrededor del mundo. El gigante asiático se esta consolidando como productor especializado de muchos mercados que representan diferentes e importantes sectores de la economía mundial, al mismo tiempo ha desarrollado masivamente su infraestructura para atender las necesidades de dichos sectores.<br><br>

A través del desarrollo de clusters empresariales, China promueve la especialización de diferentes industrias alrededor de su territorio y permite que las compañías tengan acceso fácil a tecnología y materias primas a precios preferenciales.<br><br>

De acuerdo a la encuesta que realiza anualmente el Banco Mundial, titulada “Doing Business” con fecha de Junio de 2016, sobre las ciudades que más facilidades ofrecen para hacer negocios, Hong Kong RAE (China) aparece en el cuarto puesto entre 189 países. <br><br>

Ferias internacionales como la feria de Cantón que  dos veces al año y en tres etapas congrega alrededor de 20.000 compañías en su gran mayoría Chinas y mas de 100,000 compradores de todas las partes del mundo, nos demuestra el gran potencial productor del gigante Asiático y su nivel de especialización,  las principales ciudades Chinas dan cabida a más de mil quinientas ferias especializadas en el año y son la plataforma ideal para presentar los últimos desarrollos y tendencias en diferentes mercados.<br><br>

De manera complementaria, muchas ciudades en China tienen mercados especializados que a través de empresas y comercializadores, ofrecen la mayoría de productos que componen la oferta exportadora y del mercado interno del país. Así, en mercados como el de eléctricos en ShenZhen o el monumental mercado de bienes en la ciudad de Yiwu (Fu-Tian), es posible adquirir una gran variedad de productos en cantidades pequeñas y consolidar contenedores con múltiples referencias.<br><br>

Después de más de diez años siendo actores principales en el crecimiento de nuestros clientes, representando sus intereses frente a diferentes proveedores en el continente Asiático, somos consientes de las múltiples oportunidades comerciales que ofrece la región.<br><br>

Por todas estas razones y con la seguridad de prestarle el mejor servicio de agenciamiento de compras, Smart Group Asia lo invita a importar desde China.
',
           ],
           'faq'=>[
               'title'=>'Preguntas Frecuentes',
               'title_1'=>'¿Son ciertos los Rumores sobre el factor de riesgo en las empresas Chinas?',
               'text_1'=>'China es un país bastante grande donde se puede topar con todo tipo de fraudes de parte de proveedores, fabricantes, intermediarios, revendedores, y empresas ficticias entre otros. <br><br>

El éxito en encontrar un buen proveedor es fundamental, a lo largo de nuestra experiencia nos hemos topado con supuestos proveedores que se identifican como fabricantes y al momento de realizar una visita o pedir documentación legal, descubrimos que no tienen como acreditar ningún tipo de experiencia. <br><br>

Es fundamental contar con un aliado que cuide los intereses de su empresa y lo ayude a minimizar los riesgos al momento de hacer negocios en China.',
               'title_2'=>'¿Es más caro o implica un aumento de los costos comprar un producto a través de ustedes?',
               'text_2'=>'Por el contrario, estamos seguros que a través nuestro usted lograra las mejores  condiciones económicas para su empresa, velamos por sus intereses y por el desarrollo de un producto que supere sus expectativas.',
               'title_3'=>'¿Puedo ponerme en contacto con ustedes directamente desde China?',
               'text_3'=>'Por supuesto tenemos presencia en China, Colombia, Venezuela, y Estados Unidos, no dude en ponerse en contacto con nosotros.',
               'title_4'=>'Guangzhou',
               'text_4'=>'Mobile + 86 188 188 753 21<br>
Adress: Room 2604, Building 10. Liguang Road. Hengda Yujingwan,Nanhai District, Foshan, Guangdong, China<br>
Email : gz@sga.co - acortazar@sga.co
',
               'title_6'=>'Yiwu',
               'text_6'=>'Tel +86 579 8517 5950<br>
Address: Room 347,Building Guomao - Dasha, Yiwu City, Zhejiang Province, P.R. China.<br> 
地址:中国浙江省义乌市国贸大厦347室 <br> 
Email : yiwu@sga.co - acortazar@sga.co
',
               'title_7'=>'¿Puedo contactar con el fabricante y visitar sus instalaciones o fabricas?',
               'text_7'=>'La información del proveedor es totalmente transparente hacia usted, sugerimos en la medida de lo posible conocer a sus proveedores.<br><br>

A través del servicio de auditoria y verificación de fabricas, SGA visita a su proveedor y prepara un informe completo sobre las capacidades y características de su fabricante.
.',
               'title_8'=>'¿Adquieren ustedes productos en China para revenderlo a sus clientes?',
               'text_8'=>'Somos agentes de compra, nuestra función es representar a nuestros clientes y ayudarles con sus procesos de compra, nuestro trabajo es lograr las mejores condiciones de negocio para ellos de manera transparente, permitiendo un contacto directo con los fabricantes.',
               'title_9'=>'¿Cuándo es conveniente solicitar las muestras del artículo a importar?',
               'text_9'=>'Preferimos trabajar con muestras de los bienes a comprar mientras el producto así lo permita, este proceso asegura la aprobación de parte del cliente antes de la recepción de la mercancía y nos permite tener un importante punto de referencia al momento de hacer inspecciones de calidad.',
               'title_10'=>'¿Cómo las solicito?',
               'text_10'=>'Si ha decidido solicitar las muestras, sólo tiene que avisarnos de su decisión y nos ocupamos de las gestiones con la fábrica o fábricas proveedoras en China.',
               'title_11'=>'¿Cómo me hacen llegar las muestras? ',
               'text_11'=>'Mediante el servicio de envíos rápidos a través de currier internacionales como Fedex, TNT o DHL. Dichas empresas tienen oficinas en la mayoría de los países del mundo y garantizan la seguridad del envío.',
               'title_12'=>'¿Cuánto cuesta el envío?',
               'text_12'=>'Depende del destino y de la urgencia. Puede consultar tarifas y tiempo de tránsito para su país en las páginas respectivas de las mensajerías : DHL , Fedex, TNT.',
               'title_13'=>'¿Cuánto cuesta la muestra?',
               'text_13'=>'Dependiendo mucho del precio unitario del producto final, las muestras pueden o no tener costo. Es importante tener en cuenta que el costo relacionado con muestras es insignificante respecto a la función que cumplen en el proceso de compra.',
               'title_14'=>'¿Cómo se efectúa el pago de del envío y las muestras?',
               'text_14'=>'La manera mas segura es a través nuestro, de esta manera nos encargamos de recibir, analizar, consolidar y despachar sus muestras de manera segura.
<br><br>
El flete de las muestras puede hacerse prepagándolo desde nuestra oficina o a través de su cuenta empresarial de currier internacional.
<br><br>
                            <strong>¿Cuánto cobra Smart Group Asia por gestión del envío de la muestra?</strong><br><br>
                          Absolutamente nada. Los precios del envío son transparentes, los puede verificar con la mensajería elegida.',
               
               'title_15'=>'¿Qué tipo de Servicios Ofrecen?',
               'list'=>'<li>Análisis de la competencia </li>


<li>Verificación legal de Proveedor</li>


<li>Estudio de proveedores y productos certificados:<br>


&nbsp;&nbsp;&nbsp;&nbsp;Estatus legal<br>
&nbsp;&nbsp;&nbsp;&nbsp;Catalogo de productos<br>
&nbsp;&nbsp;&nbsp;&nbsp;Políticas de calidad y soporte<br>
&nbsp;&nbsp;&nbsp;&nbsp;Precios <br>
&nbsp;&nbsp;&nbsp;&nbsp;Cantidades minimas de pedidos<br> 
&nbsp;&nbsp;&nbsp;&nbsp;Estimacion de tiempos de producción <br>
&nbsp;&nbsp;&nbsp;&nbsp;Embalaje de mercancias<br> 
&nbsp;&nbsp;&nbsp;&nbsp;Términos de negociación <br> 
&nbsp;&nbsp;&nbsp;&nbsp;Fiabilidad del proveedor<br>
&nbsp;&nbsp;&nbsp;&nbsp;Simulación de la importacion.</li>


<li>Auditoría a fábricas</li>


<li>Envío de muestras y contra muestras</li>


<li>Viajes de negocios (Sólo en China):<br>


&nbsp;&nbsp;&nbsp;&nbsp;Trámite de VISA, tiquetes aéreos y reservas hoteleras<br>
&nbsp;&nbsp;&nbsp;&nbsp;Servicio de traductores y guías<br>
&nbsp;&nbsp;&nbsp;&nbsp;Visita a ferias y fábricas</li>


<li>Agenciamiento de compra</li>


<li>Diseño de imagen corporativa, empaques y embalajes</li>


<li>Inspecciones de calidad:<br>


&nbsp;&nbsp;&nbsp;&nbsp;Inspección de calidad previo embarque<br>
&nbsp;&nbsp;&nbsp;&nbsp;Monitoreo a la producción<br>
&nbsp;&nbsp;&nbsp;&nbsp;Inspección durante la producción<br>
&nbsp;&nbsp;&nbsp;&nbsp;Inspección de producción inicial<br> 
&nbsp;&nbsp;&nbsp;&nbsp;Verificación de cargue de la mercancia<br>
&nbsp;&nbsp;&nbsp;&nbsp;Test de laboratorio y certificaciones</li>



<li>Soluciones logísticas:<br>


&nbsp;&nbsp;&nbsp;&nbsp;Almacenamiento<br>
&nbsp;&nbsp;&nbsp;&nbsp;Consolidación de mercancías<br>
&nbsp;&nbsp;&nbsp;&nbsp;Documentos para la nacionalización<br>
&nbsp;&nbsp;&nbsp;&nbsp;Fletes internacionales<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Aéreos<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- marítimos<br>
&nbsp;&nbsp;&nbsp;&nbsp;Seguros internacionales<br>
&nbsp;&nbsp;&nbsp;&nbsp;Fletes nacionales</li>


<li>Asesoría en Nacionalización de mercancías</li>


<li>Asistencia legal</li>



<p>Si tiene alguna pregunta por favor no dude en ponerse en contacto con nosotros por correo electrónico o a los números de teléfono de contacto que se encuentran en la página.</p>',
               'title_16'=>'¿Cuál es el costo de sus Servicios?',
               'text_16'=>'Nuestros servicios se pueden dividir en 5 fases:<br><br>
                            1.	Investigaciones y búsqueda de productos: La propuesta económica es de USD 300 por cada familia de productos.
<br><br>
2.	Proceso de compra : Cobramos un porcentaje sobre el valor FOB de la mercancía, que depende del monto en la compra y el tipo de compra.
<br><br>
                            3.	Planes de negocios y viajes: Se componen de:',
               'list_text2_1'=>'Investigaciones y búsqueda de productos previo al viaje: USD 300 por cada familia de productos.',
               'list_text2_2'=>'Organización logística USD 100 a USD 300.',
               'list_text2_3'=>'Acompañamiento de traductor Negociador : Este valor asciende a USD 150 x día de acompañamiento.',
               'text_17'=>'4.	Inspecciones de calidad certificadas: USD 580 a USD 800 x día de trabajo, precio para un inspector, sin importar la ubicación.<br><br> 
5.	Auditorias y verificación de proveedores: USD 800 a USD 1000 dependiendo de tamaño y ubicación de la fábrica, cubrimiento en toda China y algunos otros países de Asia.
'
               
               
           ],
           'contact'=>[
               'title'=>'Contáctenos',
               'address'=>'Dirección',
               'phone'=>'Teléfonos',
               'mail'=>'Correo electrónico',               
               'smart_group_asia'=>'SMART GROUP ASIA',
               
               'colombia'=>'Colombia (Bogotá)',
               'colombia_dir'=>'Carrera 19ª # 106a – 30, San Patricio, Bogotá, Col.',
               'colombia_tel'=>'(57 1) 551 31 31 <br>310 205 3810',
               'colombia_mail'=>'servicioalcliente@sga.co<br>acortazar@sga.co',
               
               'miami'=>'USA (Miami)',
               'miami_dir'=>'99-53 Nw 62 PL - Hialeah, Fl 33015',
               'miami_mail'=>'inquires@sga.co <br> acortazar@sga.co',
               
               'china1'=>'R.P China (Guangzhou)',
               'china1_dir'=>'Room 2604, Building 10. Liguang Road. Hengda Yujingwan,Nanhai District, Foshan, Guangdong, China',
               'china1_mail'=>'gz@sga.co <br> acortazar@sga.co',
               
               'china2'=>'R.P China (Yiwu)',
               'china2_dir'=>'Room 347,Building Guomao - Dasha, Yiwu City, Zhejiang Province, P.R. China. ',
               'china2_mail'=>'yiwu@sga.co <br> buyer3@sga.co <br> acortazar@sga.co'
               
           ],
           'services'=>[
               'title'=>'Servicios',
               'serv1'=>[
                   'title'=>'Análisis de la competencia ',
                   'text'=>''
               ],
               'serv2'=>[
                   'title'=>'Verificación legal de Proveedor',
                   'text'=>''
               ],
               'serv3'=>[
                   'title'=>'Estudio de proveedores y productos certificados',
                   'text'=>'Con el fin de analizar la viabilidad de su idea de proyecto, reunimos suficiente información sobre el fabricante y su producto y le presentamos un informe completo que le facilita la selección de su futuro proveedor y que incluye:

&nbsp &nbsp &nbsp &nbsp &nbsp - Estatus legal<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Catalogo de productos<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Políticas de calidad y soporte<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Precios <br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Cantidades minimas de pedidos <br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Estimacion de tiempos de producción <br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Embalaje de mercancias <br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Términos de negociación  <br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Fiabilidad del proveedor<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Simulación de la importacion.<br>'
               ],
               'serv4'=>[
                   'title'=>'Auditoría a fábricas',
                   'text'=>'Las auditorías a fábrica son una manera de conocer el perfil completo de su possible proveedor de forma rápida y económica, con el ánimo de convertirlo en parte fundamental de su cadena de suministro.
<br><br>
Dichas auditorias, le permiten conocer la capacidad que tiene un proveedor de producir de acuerdo a las especificaciones requeridas.
'
               ],
               'serv5'=>[
                   'title'=>'Envío de muestras y contra muestras',
                   'text'=>'Una vez que los posibles proveedores han sido seleccionados, pedimos dos conjuntos de muestras del producto final, un conjunto se envía directamente a su empresa y el otro se lo conservamos en nuestras oficinas. Estas muestras son fundamentales para tomar una decision de compra y aprobar la produccion final. '
               ],
               'serv6'=>[
                   'title'=>'Viajes de negocios (Sólo en China)',
                   'text'=>'En caso que quiera conocer a sus proveedores potenciales y su capacidad instalada, estructaramos un plan de negocios customizado que incluye:<br><br>

&nbsp &nbsp &nbsp &nbsp &nbsp - Trámite de VISA, tiquetes aéreos y reservas hoteleras<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Servicio de traductores y guías<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Visita a ferias y fábricas<br>
'
               ],
               'serv7'=>[
                   'title'=>'Agenciamiento de compra',
                   'text'=>'Ofrecemos un acompañamiento total de su orden de compra en China y los principales países de la región Asiática, minimizando los riesgos que existen al momento de hacer compras y garantizando que se cumplan los términos pactados con proveedores.'
               ],
               'serv8'=>[
                   'title'=>'Diseño de imagen corporativa, empaques y embalajes',
                   'text'=>'Nuestro departamento de diseño posee la experiencia para desarrollar su marca propia y el empaque final de su producto'
               ],
               'serv9'=>[
                   'title'=>'Inspecciones de calidad',
                   'text'=>'
                     &nbsp &nbsp &nbsp &nbsp &nbsp - Inspección de calidad previo embarque
&nbsp &nbsp &nbsp &nbsp &nbsp - Monitoreo a la producción
&nbsp &nbsp &nbsp &nbsp &nbsp - Inspección durante la producción
&nbsp &nbsp &nbsp &nbsp &nbsp - Inspección de producción inicial 
&nbsp &nbsp &nbsp &nbsp &nbsp - Verificación de cargue de la mercancia
&nbsp &nbsp &nbsp &nbsp &nbsp - Test de laboratorio y certificaciones'
               ],
               'serv10'=>[
                   'title'=>'Soluciones logísticas',
                   'text'=>'
                     &nbsp &nbsp &nbsp &nbsp &nbsp - Almacenamiento
&nbsp &nbsp &nbsp &nbsp &nbsp - Consolidación de mercancías
&nbsp &nbsp &nbsp &nbsp &nbsp - Documentos para la nacionalización
&nbsp &nbsp &nbsp &nbsp &nbsp - Fletes internacionales
&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp º 	Aéreos
&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp º 	marítimos
&nbsp &nbsp &nbsp &nbsp &nbsp - Seguros internacionales
&nbsp &nbsp &nbsp &nbsp &nbsp - Fletes nacionales'
               ],
               'serv11'=>[
                   'title'=>'Asesoría en Nacionalización de mercancías',
                   'text'=>'Para cada uno de los proyectos analizamos si existen limitaciones en términos de regulaciones, cuotas o barreras aduaneras y lo asesoramos en la nacionalización de su mercancía.'
               ],
               'serv12'=>[
                   'title'=>'Asistencia legal',
                   'text'=>'Brindamos asesoría legal en caso que desee promover su negocio en Asia, establecer una nueva empresa o tener una oficina de representación de su negocio actual.<br>
Si tiene alguna pregunta por favor no dude en ponerse en contacto con nosotros por correo electrónico o a los números de teléfono de contacto que se encuentran en la página.
'
               ]
           ]
          ];
       
       return $text[$key];
    }
    
}

