<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Lang{
 
    
    public function get($key){
        return $this->text($key);
    }
    
    private function text($key){
    
       $text= [
           'contactt'=>[
               'company'=>'Company',
               'contact'=>'Contact',
               'position'=>'Position',
               'address'=>'Address',
               'phone'=>'Phone',
               'city'=>'City'
           ],
           'top_menu'=>[
             'SGA_system'=>'Order Tracking',
             'spanish_Version'=>'Versión Español',  
             'english_version'=>'English Version'
           ],
           'menu'=>[
              'home'=>'Home',
               'services'=>'Services',
              'stages'=>'Stages',
               'success_stories'=>'Cases of success',
               'why_import_from_china'=>'Why import from China?',
               'faqs'=>'FAQ´s',
               'contact'=>'Contact Us',
               'about'=>'About us'
           ],
            'slider'=>[
                'slide_title_1'=>'What does Smart Group Asia offer to its customers?',
                'slide_text_1'=>'We offer full support in purchasing processes within the Asian continent, for raw materials, finished and semi-finished products. Our trajectory of more than ten years in the People\'s Republic of China, has allowed us to position ourselves as leaders and become an integral part of the growth of important brands within the Latin American Market.',
                
                'slide_title_2'=>'What does Smart Group Asia offer to its customers?',
                'slide_text_2'=>'Our value offer to customers is to reduce the risks related with purchases, brand developing, and procurement in Asia. We watch over the interests of our customers by representing them to their suppliers and understanding the needs of their orders, making sure to deliver the required product, in due time, with the desired quality standards and within cost expectations.',
                
                'slide_title_3'=>'What does Smart Group Asia offer to its customers?',
                'slide_text_3'=>'We follow all processes and track customer orders thru our customized system, that allows us to control over orders and keeping track of each process from the reception of the order to the product delivery at destination.'
            ],
           'header_below'=>[
               'title'=>'Grupo SGA',
               'subtitle_1'=>'Who trust us?',
               'text_1'=>'',
               'watch'=>'Watch this video',
               'read_more'=>'Read more',
               'subtitle_2'=>'Why Import from China?',
               'text_2'=>'Considering that many large companies around the world have been focusing on manufacturing in China for so long, we can understand that its factories are able to create a wide variety of products...'
               
           ],
           'aside_about'=>[
               'title_1'=>'Mission',
               'text_1'=>'Provide the best consulting services in specialized purchasing within the Asian market in order to strengthen and enrich the commercial relations between our customers and their suppliers, minimizing the risks related with purchases of goods, providing responsible, agile, and efficient assistance with the highest quality standards.',
               'title_2'=>'Vision',
               'text_2'=>'Be the leading and most innovative company of purchase consulting between Latin America and the Asian continent. We intend to become a fundamental tool for the growth and positioning of our customers and their brands, committed to the well being and development of employees and shareholders.'
           ],
           'footer'=>[
               'allies'=>'Allies with',
               'title'=>'We are your best choice',
               'text'=>'"Values such as specialization, systematization of processes, excellence, transparency and the provision of quality services with a high added value, have convert us in a specialized multinational for purchasing and guarantee the best results for your company".',
               'phone'=>'Phone',
               'contact_information'=>'Contact Us',
               'cell_phone'=>'Cell Phone',
               'email'=>'Email',
               'view_complete_information'=>'See all the information',
               'main_menu'=>'Main Menu',
               'home'=>'Home',
               'stages'=>'Stages',
               'success_stories'=>'Cases of success',
               'why_import_from_china'=>'Why import from China?',
               'faqs'=>'FAQ´s',
               'contact'=>'Contact Us',
               'commercial_references'=>'Nuestros clientes',
               'services'=>'Services',
               'order_tracking'=>'Orders tracking',
               'negotiation_with_china'=>'Negotiation with China',
               'negotiation_text_1'=>'Knowing all the stages of the negotiation process within Asia is of vital importance. Therefore, we have developed an infographic sketch that will help you visualize in detail the whole procedure from the reception of an order to the delivery of your product at destination.',
               'read_more'=>'Read more',
               'negotiation_text_2'=>'Experts in purchase services within the Asian market with an emphasis on China. We make sure of the quality of your products, minimizing risks in the purchase processes. <br><br>

We control the manufacture of your project in China from the search of a reliable supplier or product, to the safe delivery of your product.
'
               
           ],
            'home'=>[
                'our_history'=>'Our History',
                'text_history'=>'Our company, Smart Group Asia, is a purchase consulting enterprise with its headquarters in mainland China, (Yiwu and GuangZhou), and Colombia (Bogota, and Medellin).<br><br>
Our company has specialized in analyzing, developing and implementing projects related with international commerce and trade, our presence in mainland China has give us the experience to achieve total solution levels for the companies we represent.<br><br>
Our most valuable asset to achieve your company’s goal is our staff, which has been trained in procurement analysis using specialized tools, techniques, methodologies and import export best practices to conduct complete sourcing solutions. We have carefully selected a group of people that is fluent in Chinese, English and Spanish to better understand our customer’s needs and have a perfect communication with our suppliers. In China, for every two companies that have English capabilities there are eight that can communicate using Chinese only. <br><br>
Industrial Engineers, Business administrators, corporate Lawyers, and international trade assistants compose our staff.<br><br>
Presence between the Asian and Latin American Market, give us extended coverage and the opportunity to understand different culture tastes and demands.<br><br>
Smart Group Asia is ready to Engineer a sourcing solution for your business.'
            ],
            'phases'=>[
                'img_pashes'=>'images/phases_en.png',
                'img_mpashes'=>'images/m.phases_en.jpg'
            ],
            'successCase'=>[
                'title'=>'Cases of success',
                'subtitle'=>'What Our Customers Say',
                'case1'=>'<p>We chose SGA Smart Group Asia because it has allowed us to count on the support and professionalism to do business with Asia, We have made a number of imports, counting on excellent quality of our products and perfect transit times.</p>
                            <span>
                              <strong>RTI Television S.A</strong>
                              <br>Luis Alberto Monroy.<br>
                             General Manager.
                            </span>',
               'case2'=>'<p>As manufacturers and exporters, be competitive and promote the manufacturing and the employment, points in which SGA Smart Group Asia has supported us since 2005, we have made a number of imports of raw materials for the development and innovation of our products.</p>
                        <span>
                          <strong>Pettacci S.A</strong>  <br>          
                          Laureano Nunez Baron.<br>
                          General Manager.
                        </span>',
               'case3'=>'<p>SGA, provides a very professional service, which has allowed us a sustained growth since 2007, and a high level of communication and agility in imports of our raw materials and finished products. Today doing business with Asia is vital to our business and with a tool as efficient and secure as SGA Smart Group Asia we minimize all risks when making any kind of negotiation.</p>
                        <span>
                          <strong>CD Systems S.A.</strong><br>
                          Gabriel Cortzar.<br>
                          General Manager.
                        </span>',
               'case4'=>'<p>SGA Smart Group Asia is a useful tool to effectively carry out any kind of negotiations with Asia, it has offices and trained personnel to develop any type of business. One of the things we like most about SGA is its responsiveness, certified quality inspections and the security that we have sent considerable sums of money and during these 6 years we have never had any inconvenience.</p>
                        <span>
                          <strong>JG Electronics Ltda</strong><br>
                          Miguel Canal.<br>
                          General Manager.
                        </span>',
               'case5'=>'<p>We were looking for a company that would allow us to guarantee the quality of our products imported from Asia, in a professional way and thanks to SGA Smart Group Asia, we get a detailed report previous to the shipping of the merchandise and we can correct faults of production to guarantee and minimize all risk in the purchase...</p>
                        <span>
                          <strong>Industrias Humcar Ltda </strong><br><br>
                          Martin Camacho<br>
                          General Manager.
                        </span>'
               
            ],
           'whyimport'=>[
               'title'=>'Why Import from China?',
               'text_2'=>'Considering that many large companies around the world have been focusing on manufacturing in China for so long, we can understand that its factories are able to create a wide variety of products, that, through specialization, modernization of its productive processes, comparative advantages of labor and fiscal benefits, allows them to become leaders in many  markets while improving its quality.<br><br>

China stands as one of the most influential economies on the planet and presents a market full of opportunities that not only captivates but involves a number of companies around the world. The Asian giant is consolidating it self as a specialized producer of many products that represent different and important sectors of the world economy, at the same time it has massively developed its infrastructure to meet the needs of these sectors.<br><br>

Through the development of business clusters, China promotes the specialization of different industries around its territory and allows companies to have easy access to technology and raw materials at preferential prices.<br><br>

According to the World Bank\'s annual "Doing Business" survey conducted in June 2016 on the most convenient cities to do business, Hong Kong SAR (China) ranks fourth among 189 countries.<br><br>

International fairs such as the Canton Fair that twice a year and in three stages brings together about 20,000 companies in their vast majority of China and more than 100,000 buyers from all over the world, shows us the great manufacturing potential of the Asian giant and its level of expertise. Chinese main cities are home to more than fifteen hundred trade shows in the year and are the ideal platforms to present the latest developments and trends in different markets.<br><br>

In addition, many cities in China have specialized markets that, through companies and merchants, offer the majority of products that make up the country\'s export and domestic market. Thus, in street markets like the one of electronics in ShenZhen, or the monumental goods market in Yiwu City (Fu-Tian), it is possible to purchase a large variety of products in small quantities and consolidate containers with multiple references.<br><br>

After more than ten years as leading players in the growth of our customers, representing their interests in front of different suppliers in the Asian continent, we are aware of the multiple commercial opportunities offered by the region.<br><br>

For all these reasons and with the confidence of providing the best service of purchase consulting, Smart Group Asia invites you to expand your market opportunities in China.',
           ],
           'faq'=>[
               'title'=>'Frequent questions',
               'title_1'=>'Are true the rumors about making business with Chinese companies?',
               'text_1'=>'China is a fairly large country where you can come across all kinds of suppliers, manufacturers, intermediaries, resellers, traders, fictional companies etc.<br><br>

The success in finding a good supplier is fundamental, throughout our experience we have find suppliers who identify them self as manufacturers but at the moment of making a visit or requesting legal documentation, its not possible to credit any kind of experience.<br><br>

It is essential to have an ally that represent the interests of your company and help you to minimize the risks at the moment to do business in China.
',
               'title_2'=>'Is it more expensive or does it involve increasing costs to buy a product through you?',
               'text_2'=>'On the contrary, we are sure that through us you will get the best economic conditions for your company, we look after the development of a product that exceeds your expectations.',
               'title_3'=>'Can I contact you directly from China?',
               'text_3'=>'Of course, please do not hesitate to contact us. Contact details in China:',
               'title_4'=>'Guangzhou',
               'text_4'=>'Mobile + 86 188 188 753 21<br>
Adress: Room 2604, Building 10. Liguang Road. Hengda Yujingwan,Nanhai District, Foshan, Guangdong, China<br>
Email : gz@sga.co - acortazar@sga.co
',
               'title_6'=>'Yiwu',
               'text_6'=>'Phone +86 579 8517 5950<br>
Address: Room 347,Building Guomao - Dasha, Yiwu City, Zhejiang Province, P.R. China.<br> 
地址:中国浙江省义乌市国贸大厦347室 <br> 
Email : yiwu@sga.co - acortazar@sga.co
',
               'title_7'=>'Can I contact the manufacturer and visit their facilities or factories?',
               'text_7'=>'The information of the supplier is totally transparent towards you, we suggest that as far as possible you meet with your suppliers.<br><br>

Through the service of audit and verification of factories, SGA appoints your supplier and prepares a complete report on the capacities and characteristics of its manufacturer process.

.',
               'title_8'=>'Do you buy products in China to resell it to your customers?',
               'text_8'=>'We are purchasing agents, our role is to represent our customers and help them with their purchasing processes, our job is to achieve the best business conditions for them in a transparent way, allowing a direct contact with manufacturers.',
               'title_9'=>'When is it convenient to request samples of the item to be imported?',
               'text_9'=>'We prefer to work with samples of the goods to buy while the product allows it, this process ensures the approval by the customer before receiving the merchandise and allows us to have an important point of reference at the moment of making quality inspections.',
               'title_10'=>'How do I request it?',
               'text_10'=>'If you have decided to request the samples, simply notify us of your decision and we manage with the factory or supplier factories in China to receive them In our facilities.',
               'title_11'=>'How do I get samples?',
               'text_11'=>'By using international curriers like FedEx, TNT or DHL. These companies have offices in most of the countries of the world and guarantee the security of the shipment.',
               'title_12'=>'How much is the shipping?',
               'text_12'=>'It depends on fate and urgency. You can check rates and transit time for your country in the respective pages of the couriers: DHL, FedEx, TNT.',
               'title_13'=>'How much does the sample cost?',
               'text_13'=>'Depending on the unit price of the final product, samples may or may not have cost. It is important to keep in mind that the cost related to samples is insignificant with respect to the function they fulfill in the buying process.',
               'title_14'=>'How do I pay for the shipping and the samples?',
               'text_14'=>'The safest way is through us, in this way we take care to receive, analyze, consolidate and shipping your samples in a safe way.
<br><br>
The samples freight can be prepaid from our office or through your account with an international currier.
<br><br>
                            <strong>How much does Smart Group Asia collect by the sample shipping management?</strong><br><br>
                          Absolutely nothing. The shipping prices are transparent; you can verify them with the chosen courier.',
               
               'title_15'=>'What kind of Services we offer?',
               'list'=>'<li>Benchmarking of competitors</li>

<li>Legal Verification of supplier</li>

<li>Certified suppliers and product report:<br>

&nbsp;&nbsp;&nbsp;&nbsp;Sourcing Reports<br>
&nbsp;&nbsp;&nbsp;&nbsp;Import simulation</li>

<li>Supplier Audit Report</li>

<li>Sample Delivery</li>

<li>Business Trip (Only China): <br>

&nbsp;&nbsp;&nbsp;&nbsp;VISA, air tickets and Hotel Booking <br>
&nbsp;&nbsp;&nbsp;&nbsp;Translator and guide services<br>
&nbsp;&nbsp;&nbsp;&nbsp;Visits to fairs and factories</li>

<li>Purchasing agent</li>

<li>Corporate identity and packaging design</li>

<li>Quality Control:<br>

&nbsp;&nbsp;&nbsp;&nbsp;Pre-shipment inspection<br>
&nbsp;&nbsp;&nbsp;&nbsp;Production monitoring<br>
&nbsp;&nbsp;&nbsp;&nbsp;During production check<br>
&nbsp;&nbsp;&nbsp;&nbsp;Initial production Check<br>
&nbsp;&nbsp;&nbsp;&nbsp;Container loading<br>
&nbsp;&nbsp;&nbsp;&nbsp;Lab testing and Certificactions.</li>

<li>Logistics Solution:<br>

&nbsp;&nbsp;&nbsp;&nbsp;Warehousing<br>
&nbsp;&nbsp;&nbsp;&nbsp;Consolidation of containerized goods<br>
&nbsp;&nbsp;&nbsp;&nbsp;Documents for the nationalization of goods<br>
&nbsp;&nbsp;&nbsp;&nbsp;International Freight<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Air<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Sea<br>
&nbsp;&nbsp;&nbsp;&nbsp;International Insurance<br>
&nbsp;&nbsp;&nbsp;&nbsp;National Freight</li>


<li>Import goods advising (Customs Clearance)</li>



<li>Legal Assistance</li>



<p>Should you have any questions please don’t hesitate on contacting us via mail or to the telephone numbers onsite.</p>',
               'title_16'=>'What is the cost of your Services?',
               'text_16'=>'Our services can be divided into 5 phases:<br><br>

1.	Research and product search (Procurement): The economic proposal is USD 300 for each family of products.<br><br>

2.	Purchase process: We charge a percentage on the FOB value of the merchandise, which depends on the amount in the purchase and the type of purchase.<br><br>

3.	Business and travel plans: They consist of:<br><br>
',
               'list_text2_1'=>'Research and product search pre-trip: USD 300 for each product family.',
               'list_text2_2'=>'Logistics organization USD 100 to USD 300.',
               'list_text2_3'=>'Accompaniment of translator Negotiator: This value amounts to USD 150 x day of accompaniment.',
               'text_17'=>'4.	Certified quality inspections: USD 580 to USD 800 x workday, price for an inspector, regardless of location.<br><br>

5.	Supplier audits and verification: USD 800 to USD 1000 depending on size and location of the factory, covering all of China and some other Asian countries.'
               
               
           ],
           'contact'=>[
               'title'=>'Contact Us',
               'address'=>'Address',
               'phone'=>'Phones',
               'mail'=>'Email',               
               'smart_group_asia'=>'SMART GROUP ASIA',
               
               'colombia'=>'Colombia (Bogotá)',
               'colombia_dir'=>'Carrera 19ª # 106a – 30, San Patricio, Bogotá, Col.',
               'colombia_tel'=>'(57 1) 551 31 31 <br>310 205 3810',
               'colombia_mail'=>'servicioalcliente@sga.co<br>acortazar@sga.co',
               
               'miami'=>'USA (Miami)',
               'miami_dir'=>'99-53 Nw 62 PL - Hialeah, Fl 33015',
               'miami_mail'=>'inquires@sga.co <br> acortazar@sga.co',
               
               'china1'=>'R.P China (Guangzhou)',
               'china1_dir'=>'Room 2604, Building 10. Liguang Road. Hengda Yujingwan,Nanhai District, Foshan, Guangdong, China',
               'china1_mail'=>'gz@sga.co <br> acortazar@sga.co',
               
               'china2'=>'R.P China (Yiwu)',
               'china2_dir'=>'Room 347,Building Guomao - Dasha, Yiwu City, Zhejiang Province, P.R. China. ',
               'china2_mail'=>'yiwu@sga.co <br> buyer3@sga.co <br> acortazar@sga.co'
               
           ],
           'services'=>[
               'title'=>'Services',
               'serv1'=>[
                   'title'=>'Benchmarking of competitors',
                   'text'=>''
               ],
               'serv2'=>[
                   'title'=>'Legal Verification of supplier',
                   'text'=>''
               ],
               'serv3'=>[
                   'title'=>'Certified suppliers and product report',
                   'text'=>'For us every assignment is treated as a project, after analyzing the feasibility of it, we gather enough information about the manufacturer, its product, quality and government certification, pricing, order quantities, estimates of production times, packing, terms of negotiation, and overall reliability of the supplier, in order to present a complete inform to our customers to ease the process of selecting the manufacturer.<br><br>

&nbsp &nbsp &nbsp &nbsp &nbsp - Sourcing Reports<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Import simulation'
               ],
               'serv4'=>[
                   'title'=>'Supplier Audit Report',
                   'text'=>'Manufacturing Audits are a cost and time-effective way to prepare a complete profile of your potential supplier, making sure they will become a reliable part of your supply chain. <br><br>

Audit Reports enables you to confirm that the audited site is capable of producing in terms of your specifications.'
               ],
               'serv4'=>[
                   'title'=>'Sample Delivery',
                   'text'=>'Once the supplier has been selected we request two sets of samples of the final product, one set is directly sent to our customers and the other one is sent to us. This sample sets are critical to approve an order and start the negotiation process and furthermore to do the quality control inspection after the production is ready.
'
               ],     
               'serv5'=>[
                   'title'=>'Business Trip (Only China) ',
                   'text'=>'We will prepare a business agenda in case you want to personally contact your future suppliers that include:<br><br>

 &nbsp &nbsp &nbsp &nbsp &nbsp -  VISA, air tickets and Hotel Booking <br>
 &nbsp &nbsp &nbsp &nbsp &nbsp -  Translator and guide services<br>
 &nbsp &nbsp &nbsp &nbsp &nbsp -  Visits to fairs and factories<br>'
               ],
               'serv6'=>[
                   'title'=>'Purchasing agent',
                   'text'=>'We can assist you with total procurement in China and major countries within the Asian region, finding reliable suppliers capable of fulfilling your companies demand at convenient prices and within the time and quality expected.<br><br>

Perhaps, the most important step within the project consecution is negotiating its terms with the supplier. Our staff has been trained to negotiate considering cultural facts that are critical in order to obtain better conditions.
'
               ],
               'serv7'=>[
                   'title'=>'Corporate identity and packaging design',
                   'text'=>''
               ],
               'serv8'=>[
                   'title'=>'Quality Control',
                   'text'=>'&nbsp &nbsp &nbsp &nbsp &nbsp - Pre-shipment inspection<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Production monitoring<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - During production check<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Initial production Check<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Container loading<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Lab testing and Certificactions.
'
               ],
               'serv9'=>[
                   'title'=>'Logistics Solution',
                   'text'=>'&nbsp &nbsp &nbsp &nbsp &nbsp - Warehousing<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Consolidation of containerized goods<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - Documents for the nationalization of goods<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - International Freight<br>
&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp º Air<br>
&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp º Sea<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - International Insurance<br>
&nbsp &nbsp &nbsp &nbsp &nbsp - National Freight'
               ],
               'serv10'=>[
                   'title'=>'Import goods advising (Customs Clearance)',
                   'text'=>'For each of the projects we analyze whether there are limitations in terms of regulations, quotas, customs barriers etc, within the parts, in order to assure the viability of them.'
               ],
               'serv11'=>[
                   'title'=>'Legal Assistance',
                   'text'=>'Our lawyers have all the experience you need in case you want to promote your business in Asia by the means of establishing a new company or having a rep-office of your current business.
<br><br>
Should you have any questions please don’t hesitate on contacting us via mail or to the telephone numbers onsite.'
               ]                
           ]
          ];
       
       return $text[$key];
    }
    
}

