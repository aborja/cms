/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$('#changeLang').on('click', function(e){
   e.preventDefault();
   var lang=$(this).data('lang');
   $.ajax({
      url:'?a=setLanguage',
      type:'post',
      data:{'lang':lang},
      dataType:'Json',
      success:function(data){
         if(data.success=="true"){
             location.reload();
         } 
      }
   });
   
});
