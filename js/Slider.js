var sliderNum;
var ani = ani1;
var easing = ['easeInCubic', 'easeOutCubic'];
var imgTime = 2000;
var txTime = 1000;
var betweenImg = imgTime + txTime + 6000;
var ini = 0;
var out;
var status = 0;
var ori = "right";

function anInt() {
  Int = setInterval(function () {
    ani();
  }, betweenImg);
}

$(window).load(function () {
  anInt();
});

$(document).ready(function (e) {

  $("header #sliderBox .Arrow1").click(function () {
    clearInterval(Int);
    ori = "left";
    ani();
    anInt();
  });
  $("header #sliderBox .Arrow2").click(function () {
    clearInterval(Int);
    ori = "right";
    ani();
    anInt();
  });
  ani();
});

function Ori() {
  // To Left
    if (ori == "left" && (out < ini)) {
        out = ini + 1;
    }
    if (ori == "left") {
        if (out === sliderNum && ini === 1) {
            out = 1;
        } else {
            out = out === 1 ? sliderNum : out - 1;
        }
        ini = ini === 1 ? sliderNum : ini - 1;
    }
    
    // To Right
    if (ori == "right" && (ini < out)) {
        out = ini - 1;
    }
    if (ori == "right") {
        if (out === 1 && ini == sliderNum) {
            out = sliderNum;
        } else {
            out = out === sliderNum ? 1 : out + 1;
        }
        ini = ini === sliderNum ? 1 : ini + 1;
    }
}

function ani1() {
  sliderNum = $("header #sliderBox ul li").length;

  if (status == 0) {
    status = 1;
    out = sliderNum;
    $("header #sliderBox ul li figure").css({
      transform: 'scale(1.5) rotate(10deg)',
      opacity: 0
    });
    $("header #sliderBox ul li .txtContainer span").css({
      x: 0,
      opacity: 1
    });
  }

  Ori();

  // In Animation

  // Visibility Box
  $("header #sliderBox ul li:nth-child(" + ini + ")").stop()
          .transition({'visibility': 'visible'}, imgTime, easing[1]);
  // Img
  $("header #sliderBox ul li:nth-child(" + ini + ") figure").stop()
          .transition({'opacity': '1', scale: '1.0', rotate: '0deg'}, imgTime, easing[1]);
//  // Txts 
//  $("header #sliderBox ul li:nth-child(" + ini + ") .txtContainer").stop()
//          .transition({'opacity': '1'}, txTime, easing[1]);
//  $("header #sliderBox ul li:nth-child(" + ini + ") .txtContainer span").stop()
//          .transition({x: 0, 'opacity': '1'}, txTime, easing[1]);

  // Out Animation

  // Visibility Box
  $("header #sliderBox ul li:nth-child(" + out + ")").stop()
          .transition({'visibility': 'hidden'}, imgTime, easing[1]);
  // Img
  $("header #sliderBox ul li:nth-child(" + out + ") figure").stop()
          .transition({'opacity': '0', scale: '1.5', rotate: '10deg'}, imgTime, easing[1]);
//  // Txts
//  $("header #sliderBox ul li:nth-child(" + out + ") .txtContainer").stop()
//          .transition({'opacity': '0'}, txTime, easing[1]);
//  $("header #sliderBox ul li:nth-child(" + out + ") .txtContainer span").stop()
//          .css({x: -50, 'opacity': '0'}, txTime, easing[1]);

  ori = "right";
}